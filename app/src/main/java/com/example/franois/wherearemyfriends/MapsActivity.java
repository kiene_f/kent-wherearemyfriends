package com.example.franois.wherearemyfriends;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    // Class User to convert JSON
    static class User {
        String name;
        String lon;
        String lat;
    }

    static class Get {
        List<User> Users;
    }


    private void getMyFriends(GoogleMap googleMap) {
        mMap = googleMap;
        String url = "https://www.cs.kent.ac.uk/people/staff/iau/LocalUsers.php";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Volley GET Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Convert JSON to Gson
                        Gson gson = new Gson();
                        Get get = gson.fromJson(response.toString(), Get.class);

                        // Add each user (friend) position with blue icon
                        for (User user : get.Users)
                        {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(user.lat), Double.parseDouble(user.lon))).title(user.name).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error", "Error in the getRequest");
                    }
                }
        );

        queue.add(getRequest);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Create my position and set default zoom
        LatLng me = new LatLng(51.297407, 1.069594);
        mMap.addMarker(new MarkerOptions().position(me).title("Me"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(me));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(me, 15.5f));

        // Add Friends position
        getMyFriends(googleMap);
    }
}
